require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  assetPrefix: process.env.PATH_PREFIX || '/',
  publicRuntimeConfig: {
    basePath: process.env.PATH_PREFIX || '/',
  },
};
